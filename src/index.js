const express = require('express')
const morgan = require('morgan');
const mongoose = require('mongoose');

require('dotenv').config();

const app = express()

mongoose.connect(
  process.env.MONGODB_URL,
  { useNewUrlParser: true, useUnifiedTopology: true }
)

app.use(express.json())

app.use(express.urlencoded({ extended: true }))

app.use(morgan('dev'))

app.use('/files', express.static(__dirname + '/tmp/uploads'));

app.use(require('./routes'))

app.listen(3000)
