const mongoose = require('mongoose');
const aws = require('aws-sdk');

const s3 = new aws.S3();

const PostSchema = new mongoose.Schema({
  name: String,
  size: Number,
  key: String,
  url: String,
  createdAt: {
    type: Date,
    default: Date.now
  }
});

PostSchema.pre('save', function () {
  if(!this.url) {
    console.log(process.env.APP_URL)
    this.url = `${process.env.APP_URL}/files/${this.key}`
  }
})

PostSchema.pre('remove', function() {
  if(process.env.STORAGE_TYPE === 's3') {
    return s3.deleteObject({
      Bucket: 'upload-example-test',
      Key: this.key
    }).promise()
  }
})

module.exports = mongoose.model('Post', PostSchema)